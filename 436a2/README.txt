Joshua pudrycki
jdp877	11153313

CMPT 436 A3

// A Chat room using CORBA.

1. Using command prompt navigate to directory consisting of ChatSystem.idl (along with numerous other java classes)
2. >orbd -ORBInitialPort 1050  // this will launch the orb

3. >idlj -fall ChatSystem.idl             //this will build a folder of java files using Corba shinanigans
4. >javac *.java chatSystem\*.java        //this will compile all java files in current directory as well as the ones newly created by idlj
5. >java ChatRoomServer -ORBInitialPort 1050 -ORBInitialHost localhost      // this will launch the server
6. >java ChatRoomClient -ORBInitialPort 1050 -ORBInitialHost localhost      // this will create a client.  can be done multiple times to create more clients

note: on window command line steps 2 and 5 will need to preceeded with 'start'
      I personally prefer to use 'start' for step 6 as well as it launches it in its' own window



Chat commands as client: 

-create <room>  // creates a chat
-enter <room>   //joins a chat room 
-leave         // leaves the current chat room
-list          //lists all chat rooms
-name <username>  // used to changer username
-help             //displays available commands
-quit             //terminates program

note: [ctrl+c] can also be used to terminate programs on the command line


