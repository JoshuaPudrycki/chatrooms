
import chatSystem.*;
import chatSystem.ChatSystem;
import chatSystem.ChatSystemHelper;

import org.omg.CORBA.ORB;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;

import java.util.Scanner;

public class ChatRoomClient
{
    static String prefix = "-";
    static String create = prefix + "create ";
    static String list   = prefix + "list";
    static String enter   = prefix + "enter ";
    static String leave  = prefix + "leave";
    static String help   = prefix + "help";
    static String setname   = prefix + "setname ";
    static String quit   = prefix + "quit";

    static ChatSystem chatSystemImpl;
    static String username;

    public static class Input implements Runnable {

        public void run() {
            Scanner in = new Scanner(System.in);
            while (true) {
                String s = in.nextLine();
                parse(s);
            }
        }

        void parse(String str) {
            if (str.startsWith(create)) {
                String name = str.substring(create.length());
                chatSystemImpl.createChatRoom(username, name);
            } else if (str.startsWith(list)) {
                chatSystemImpl.listChatRooms(username);
            } else if (str.startsWith(enter)) {
                String name = str.substring(enter.length());
                chatSystemImpl.joinChatRoom(username, name);
            } else if (str.startsWith(leave)) {
                chatSystemImpl.leaveChatRoom(username);
            } else if (str.startsWith(help)) {
                help();
            } else if (str.startsWith(setname)) {
                String name = str.substring(setname.length());
                chatSystemImpl.changeName(username, name);
            } else if (str.startsWith(quit)) {
                System.exit(0);
            } else {
                chatSystemImpl.sendMessage(username, str);
            }
        }
		
		//displays possible commands
        void help() {
            String str = "Commands:\n" +
                    create + "\n" +
                    list + "\n" +
                    enter + "\n" +
                    leave + "\n" +
                    help + "\n" +
                    setname + "\n" +
                    quit;
            System.out.println(str);
        }
    }

    public static class Output implements Runnable {
		//Thread is given to the client.  Runs until terminated.  
        public void run() {
            while(true) {
                String message = chatSystemImpl.getMessage(username);
                if (!message.isEmpty()) {
                    System.out.println(message);
                } else {
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public static void main(String args[])
    {
        try{
            //Create ORB stuff
			//https://docs.oracle.com/javase/7/docs/technotes/guides/idl/tutorial/GSapp.html
            ORB orb = ORB.init(args, null);
            org.omg.CORBA.Object objRef =
                    orb.resolve_initial_references("NameService");

            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

            String name = "ChatSystem";
            chatSystemImpl = ChatSystemHelper.narrow(ncRef.resolve_str(name));

            System.out.println("Obtained a handle on server object: " + chatSystemImpl);
            username = chatSystemImpl.connect();
			System.out.println();
			
			//Intro text
			System.out.println("Welcome!  Please create username.");
			System.out.println("Usernames are created using the format: -setname <username>");
			
			//spawn initial threads
            new Thread(new Input()).start();
            new Thread(new Output()).start();
			;

        } catch (Exception e) {
            System.out.println("ERROR : " + e) ;
            e.printStackTrace(System.out);
        }
    }

}
