//Used to store the content of messages, along with which user said it.

public class Message {
    String username;
    String text;

    public Message(String username, String text) {
        this.username = username;
        this.text = text;
    }

    public String toString() {
        
        return  ( username + ": " + text);
    }
}
