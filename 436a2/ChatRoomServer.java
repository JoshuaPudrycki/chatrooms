
import chatSystem.ChatSystem;
import chatSystem.ChatSystemHelper;
import chatSystem.ChatSystemPOA;
import org.omg.CORBA.ORB;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

import java.util.ArrayList;

//creates and runs ORB
//ensures proper command are entered by user
class ChatSystemImpl extends ChatSystemPOA {

    private ORB orb;
    Server server = new Server();

    public void setORB(ORB orb_val) {
        orb = orb_val;
    }

    public String connect() {
        return server.addUser();
    }

    public void sendMessage(String username, String message) {
        User user = server.getUser(username);
        if (user != null) {
            String chatName = user.getChatRoom();
            ChatRoom chatRoom = server.getChatRoom(chatName);
            if (chatRoom != null) {
                String chatMessage = (username+": "+message);
                chatRoom.addMessage(user, chatMessage);
            } else {
                user.addMessage("Error: Please enter a chatroom to send messages");
            }
        } else {
            System.out.println("Invalid username given for sending message.");
        }
    }

    public String getMessage(String usernames) {
        User user = server.getUser(usernames);
        if (user != null) {
            String message = user.getMessage();
            if (message != null) {
                return message;
            } else {
                return "";
            }
        } else {
            System.out.println("Invalid username given for receiving message.");
            return "";
        }
    }

    public void createChatRoom(String username, String name) {
        User user = server.getUser(username);
        if (user != null) {
            server.addChatRoom(name);
            user.addMessage("Notice: Created " + name + ".");
        } else {
            System.out.println("Invalid username given for creating chat room.");
        }
    }

    public void listChatRooms(String username) {
        User user = server.getUser(username);
        if (user != null) {
            ArrayList<String> chatNames = server.getChatRooms();
            String str = "Chat Rooms:\n";
            for (String chatName : chatNames) {
                str += chatName + "\n";
            }
            user.addMessage(str.substring(0, str.length()-1));
        } else {
            System.out.println("Invalid username given for listing chat rooms.");
        }
    }

    public void joinChatRoom(String username, String name) {
        User user = server.getUser(username);
        if (user != null) {
            ChatRoom chatRoom = server.getChatRoom(name);
            if (chatRoom != null) {
                user.addMessage("Notice: Joined " + name +  ".");
                chatRoom.addUser(user);
            } else {
                user.addMessage("That room does not exist!");
            }
        } else {
            System.out.println("Invalid username given for joining chat room.");
        }
    }

    public void leaveChatRoom(String username) {
        User user = server.getUser(username);
        if (user != null) {
            String chatName = user.getChatRoom();
            ChatRoom chatRoom = server.getChatRoom(chatName);
            chatRoom.removeUser(user);
            user.addMessage("Notice: Left " + chatName +  ".");
        } else {
            System.out.println("Invalid username given for leaving chat room.");
        }
    }
	
	public void changeName(String username, String name) {
		User user = server.getUser(username);
		if (user != null) {
			user.setName(name);
			user.addMessage("Notice: Changed name to " + name +  ".");
		} else {
		}
    }
}

public class ChatRoomServer {
    public static void main(String args[]) {
        try{
           //Create ORB stuff
			//See https://docs.oracle.com/javase/7/docs/technotes/guides/idl/tutorial/GSserver.html for documentation.
            ORB orb = ORB.init(args, null);
            POA rootpoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
            rootpoa.the_POAManager().activate();
            ChatSystemImpl chatSystemImpl = new ChatSystemImpl();
            chatSystemImpl.setORB(orb);
            org.omg.CORBA.Object ref = rootpoa.servant_to_reference(chatSystemImpl);
            ChatSystem href = ChatSystemHelper.narrow(ref);

            org.omg.CORBA.Object objRef =
                    orb.resolve_initial_references("NameService");
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);
            String name = "ChatSystem";
            NameComponent path[] = ncRef.to_name( name );
            ncRef.rebind(path, href);

            System.out.println("server.ChatRoomServer ready and waiting ...");

            orb.run();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("server.ConnServer Exiting ...");

    }
}
