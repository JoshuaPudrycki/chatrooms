
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.*;


//holds data for user: their name along with what chat room they are currently in

//can only be in a single chat room at a time
public class User {
	//for locks
    ReadWriteLock lock = new ReentrantReadWriteLock();
	//saves all messages
    Queue<String> messages = new LinkedList<String>();
	
	//variables
	String username;
    String chatRoom;

    public User(String name) {
        this.username = name;
    }



    public String getUsername() {
        lock.readLock().lock();
        String name = this.username;
        lock.readLock().unlock();
        return name;
    }

    public void setName(String name) {
        lock.writeLock().lock();
        this.username = name;
        lock.writeLock().unlock();
    }

    public String getChatRoom() {
        lock.readLock().lock();
        String chatRoom = this.chatRoom;
        lock.readLock().unlock();
        return chatRoom;
    }

    public void setChatRoom(String chatRoom) {
        lock.writeLock().lock();
        this.chatRoom = chatRoom;
        lock.writeLock().unlock();
    }

    public void addMessage(String message) {
        lock.writeLock().lock();
        messages.add(message);
        lock.writeLock().unlock();
    }

    public String getMessage() {
        lock.writeLock().lock();
        String message = messages.poll();
        lock.writeLock().lock();
        return message;
    }
}
