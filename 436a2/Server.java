
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;


//Holds all relevant data
//all users and all chat rooms

//holds functions that manipulate the hashmaps of all current users + chatrooms
public class Server {
    static final String currentName = "";

    ReadWriteLock lock = new ReentrantReadWriteLock();
    HashMap<String, User> users = new HashMap<String, User>();
    HashMap<String, ChatRoom> chatRooms = new HashMap<String, ChatRoom>();
    

    public void addChatRoom(final String name) {
        lock.writeLock().lock();
        ChatRoom chatRoom = new ChatRoom(name);
        chatRooms.put(chatRoom.getName(), chatRoom);
        lock.writeLock().unlock();

        new Thread(new Runnable() {
            public void run() {
                ChatRoom chatRoom = getChatRoom(name);
			}
        }).start();
    }

    public ChatRoom getChatRoom(String name) {
        lock.readLock().lock();
        ChatRoom chatRoom = chatRooms.get(name);
        lock.readLock().unlock();
        return chatRoom;
    }

    public void removeChatRoom(String name) {
        lock.writeLock().lock();
        ChatRoom chatRoom = chatRooms.remove(name);
        chatRoom.delete();
        lock.writeLock().unlock();
    }

    public ArrayList<String> getChatRooms() {
        lock.readLock().lock();
        ArrayList<String> names = new ArrayList<String>(chatRooms.keySet());
        lock.readLock().unlock();
        return names;
    }

    public String addUser() {
        lock.writeLock().lock();
        User user = new User(currentName);
        users.put(user.getUsername(), user);
        lock.writeLock().unlock();
        return "";
    }

    public User getUser(String name) {
        lock.readLock().lock();
        User user = users.get(name);
        lock.readLock().unlock();
        return user;
    }


}
