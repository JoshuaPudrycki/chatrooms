
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;


//Object that consists of two lists
// 1) Holds dictionary of all users in chatroom
// 2) Holds list messages
public class ChatRoom {
    ReadWriteLock lock = new ReentrantReadWriteLock();
    final String name;
    HashMap<String, User> users = new HashMap<String, User>();
    ArrayList<String> messages = new ArrayList<String>();


    public ChatRoom(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addMessage(User user, String message) {
        lock.writeLock().lock();
  
        messages.add(message);
        for (String s : users.keySet()) {
            users.get(s).addMessage(message.toString());
        }
        lock.writeLock().unlock();
    }

    public ArrayList<String> getMessages() {
        lock.readLock().lock();
        ArrayList<String> copy = new ArrayList<String>(messages);
        lock.readLock().unlock();
        return copy;
    }

    public void addUser(User user) {
        lock.writeLock().lock();
        
            users.put(user.username,user);
            user.setChatRoom(this.name);
            for (String message : messages) {
                user.addMessage(message.toString());
            }
       
        lock.writeLock().unlock();
    }

    public void removeUser(User user) {
        lock.writeLock().lock();
        users.remove(user);
        user.setChatRoom(null);
        lock.writeLock().unlock();
    }

	public void delete() {
		lock.writeLock().lock();
		for (String s : users.keySet())
		{
			User user = users.get(s);
			user.setChatRoom(null);
			user.addMessage("Chatroom deleted!");
		}
    lock.writeLock().unlock();
	}

}
