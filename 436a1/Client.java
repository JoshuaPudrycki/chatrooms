import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Scanner;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.ArrayList;


//Client in TCP Server-Client Chatroom
public class Client {
	
	//service stuff
	private Socket socket;
	private PrintStream printStream;
	private BufferedReader bufferedReader;

	private String username;
	private String room;
	
	
	
	//global variables
	String ipAddress = "127.0.0.1";
	int port = 6969;
	boolean inRoom;
	

	//Constructor for client.  Sets up socket, and starts user in the lobby.
	public Client() {
		runSocket();
		inRoom=false;
	}
	

	//Sets up scanner and interface
	public void Run() {
        
		//Initializes variables on entry
		String input;
		Scanner scanner = new Scanner(System.in);
		
		username = null;

		
		

        //Creates user.  Adds user to user dictionary
        this.print("Please enter your username: ");
        input = scanner.nextLine();
        while (input.isEmpty()) {
            this.print("Cannot have blank username!");
            input = scanner.nextLine();
        }

        boolean success = CreateUser(input);
        while (!success) {
            System.out.println("Failed to create user");

            this.print("Please enter a valid username: ");
            input = scanner.nextLine();
            while (input.isEmpty()) {
                this.print("Cannot have blank username!");
                input = scanner.nextLine();
            }

            success = CreateUser(input);
        }
        System.out.println("User successfully created");

        //Reads commands until user types "quit" 
		if(inRoom==false);
		{
			this.print(this.getPrompt());
		}
        input = scanner.nextLine();
        while (!input.equals("quit")) {
            this.handleInput(input);
			if(inRoom==false);
			{
				this.print(this.getPrompt());
			}
            input = scanner.nextLine();
        }

       //Returns to lobby then closes socket
        LeaveRoom();
        CloseSocket();
    }

   //Asks the user for input
   //Is supposed to only work when user is in lobby, but seems to prompt even when in chatroom ..?
    private String getPrompt() {
		
        String prompt = "In lobby, please select option: ";
        String room = currentRoom();

        if (room != null) {
            return "Currently in room(" + room + "): ";
        }
        return prompt;
    }

    //Reads userinput.  
	//Checks if command is valid.
	//Respond to command
    private void handleInput(String input) {
        Scanner scanner = new Scanner(new ByteArrayInputStream(input.getBytes()));

        //Scans for command from user
        String command;
        if (scanner.hasNext()) {
            command = scanner.next().trim();
        } else {
            return;
        }

        //Does appropriate
		//Checks input through series of if/else statements
        String argument;
        boolean success;
        ArrayList<String> results;
	
		if(command.equals("-list"))
		{
			System.out.println("Current rooms are: ");
            results = getRooms();
            if (results != null) {
                results.forEach(s -> System.out.println(s));
			} else {
                    this.errorln("Failed to list the existing rooms");
            }
		}
		
		else if(command.equals("-create"))
		{
			argument = scanner.nextLine().trim();
            success = CreateRoom(argument);
            System.out.println(success ? "Room successfully created" : "Failed to create room");
		}
		
		else if (command.equals("-enter"))
		{
			argument = scanner.nextLine().trim();
			results = JoinRoom(argument);
			if (results != null) {
				results.forEach(s -> System.out.println(s));
			} else {
				this.errorln("Failed to join the room");
			}
			inRoom = true;
		}
		//used for sending messages
		//currently prints entire chatroom history everytime message is sent
		//must be in a chatroom to send a message
		//currently requires a ' ' (a space) from user after the > to send input
		else if(command.charAt(0)=='>')
		{
			if(inRoom)
			{
				argument = scanner.nextLine().trim();
				success = SendMessage(argument);
				
				results = getChatRoomMessages();
				if(results!=null)
				{
					for(int i =0; i<results.size(); i++)
						
					System.out.println(results.get(i));
				}
			}
			else 
			{
				System.out.println("Please enter a chatroom to chat!");
			}
		}
		
		else if (command.equals("-help"))
		{
			
			printOptions();
		}
		
		else if (command.equals("-leave"))
		{
			success = LeaveRoom();
			System.out.println(success ? "Room successfully left" : "Failed to leave room");
			inRoom=false;	
		}			
		else 
		{
			System.out.println("Incorrect input.  Commands are: ");
			printOptions();
		}
	}
    /* Print a message without a newline */
    private void print(String message) {
        System.out.print(message);
    }

    /* Print a new message with a newline */
    private void println(String message) {
        System.out.println(message);
    }

    /* Print an error with a newline */
    private void errorln(String error) {
        System.err.println(error);
    }
	
	private void printOptions(){
			System.out.println();
			System.out.println("> <message>");
		    System.out.println("-create <room name>");
            System.out.println("-enter <room name>");
            System.out.println("-list");
            System.out.println("-leave");
			System.out.println("-help");
			System.out.println("quit");
			System.out.println();
	}
	
	private void runSocket(){
		try {
            this.socket = new Socket(ipAddress, port);
        } catch (Exception e) {
            System.err.println("Error: Failed to create socket connection");
        }

        try {
            this.bufferedReader = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            this.printStream = new PrintStream(this.socket.getOutputStream());
        } catch (Exception e) {
            System.err.println("Error: Failed to create socket connection");
        }
    }
	
	 public String currentRoom() {
        return this.room;
    }
	
	public boolean CreateUser(String user) {
        this.printStream.println("user");
        this.printStream.println(user);

		boolean success;
        String response;
        
        try {
            response = this.bufferedReader.readLine();
            success = Boolean.parseBoolean(response);
            this.username = success ? user : null;
            return success;
        } catch (Exception e) {
        }

        return false;
    }
	
	public boolean CreateRoom(String room) {
        this.printStream.println("create");
        this.printStream.println(room);

        boolean success;
        String response;
        try {
            response = this.bufferedReader.readLine();
            success = Boolean.parseBoolean(response);
            return success;
        } catch (Exception e) {
        }

        return false;
    }
	
	/* Join a chat room on the chat room server and retrieve all existing messages */
    public ArrayList<String> JoinRoom(String room) {
        this.printStream.println("enter");
        this.printStream.println(room);
        this.printStream.println(this.username);

        String response;
        boolean success;
        int numMsgs;
        ArrayList<String> messages;
        try {
            response = this.bufferedReader.readLine();
            success = Boolean.parseBoolean(response);
            if (!success) {
                return null;
            }

            numMsgs = Integer.parseInt(this.bufferedReader.readLine());
            if (numMsgs < 0) {
                return null;
            }

            messages = new ArrayList<String>();
            for (int c = 0; c < numMsgs; c += 1) {
                messages.add(this.bufferedReader.readLine());
            }

            this.room = room;
            return messages;
        } catch (Exception e) {
            System.err.println("error: join - failed to receive response from server");
        }

        return null;
    }
	
	/* Leave the user's current chat room on the chat room server */
    public boolean LeaveRoom() {
        this.printStream.println("leave");
        this.printStream.println(this.username);

        boolean success;
        try {
            success = Boolean.parseBoolean(this.bufferedReader.readLine());

            if (success) {
                this.room = null;
            }
            return success;
        } catch (Exception e) {
            System.err.println("Error: no response from server");
        }

        return false;
    }
	
	//returns all chatrooms on the server
    public ArrayList<String> getRooms() {
        this.printStream.println("list");

        String newMsg;
        boolean success;
        int numberOfRooms;
        ArrayList<String> rooms;
        try {
            newMsg = this.bufferedReader.readLine();
            success = Boolean.parseBoolean(newMsg);
            if (!success) {
                return null;
            }

            numberOfRooms = Integer.parseInt(this.bufferedReader.readLine());
            if (numberOfRooms < 0) {
                return null;
            }
			//array list storing rooms
            rooms = new ArrayList<String>();
            for (int c=0; c < numberOfRooms; c ++) {
                rooms.add(this.bufferedReader.readLine());
            }
            return rooms;
        } catch (Exception e) {
            System.err.println("Error: no response from server");
        }
        return null;
    }
	
	//sends message to server, which will save it in the chatroom text-log
    public boolean SendMessage(String message) {
        this.printStream.println("send");
        this.printStream.println(this.username);
        this.printStream.println(message);

        boolean success;
        try {
            success = Boolean.parseBoolean(this.bufferedReader.readLine());
            return success;
        } catch (Exception e) {
            System.err.println("Error: 'send' failed");
        }

        return false;
    }
	
	//gets all messages in chatroom
    public ArrayList<String> getChatRoomMessages() {
        this.printStream.println("update");
        this.printStream.println(this.username);

        String newMsg;
        boolean success;
        int numberOfMessages;
        ArrayList<String> messages;
        try {
            newMsg = this.bufferedReader.readLine();
            success = Boolean.parseBoolean(newMsg);
            if (!success) {
                return null;
            }

            numberOfMessages = Integer.parseInt(this.bufferedReader.readLine());
            if (numberOfMessages < 0) {
                return null;
            }

            messages = new ArrayList<String>();
            for (int c=0; c < numberOfMessages; c ++) {
                messages.add(this.bufferedReader.readLine());
            }

            return messages;
        } catch (Exception e) {
        }

        return null;
    }
	
	 /* End the current connection with the chat room server */
    public void CloseSocket() {
        try {
            this.socket.close();
        } catch (Exception e) {
            System.err.println("Error: socket failed to close properly");
        }
    }
	
	
	

	public static void main(String[] args) {
		Client client = new Client();
		client.Run();
		
	}
}