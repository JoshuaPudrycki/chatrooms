//Joshua Pudycki 
//11153313  jdp877

A Client-Server Chatroom built using sockets.
Client done in Java.
Server done in C#.  



How to run Client Server System using command line:

Server:  
1) Go to directory consisting of Server.cs
2) Compile Server.cs
	>csc Server.cs
3)Run Server execuatable
	>Server

Client:
1) Go to directory consisting of Client.java
2) Compile Client.java
	>javac Client.java
3) Run Client Executable
	>java Client

About the chat system:
	As the Client:
	- Use "-help" to see available options.
	- Use "quit" to exit program

	When in a chatroom chat using:
	"> This is text"
	The ">" as well as the space after it are necessary.

	Chatroom text will be updated any time you send a message.


Ctrl+C can be used to close Client/Server.
	
	
