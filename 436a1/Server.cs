using System;
using System.Net;
using System.Net.Sockets;
using System.IO;

using System.Threading;
using System.Collections.Generic;


namespace server
{
   //Server in TCP Server-Client Chatroom
    public class Server
    {
		
		//stores all users
		private Dictionary<string, string> users;
		//stores all rooms
		private Dictionary<string, List<string>> rooms;
        int port = 6969;
		IPAddress ip;
     
		
		
		public Server(){
			//initializes Dictionaries to hold data
			users = new Dictionary<string, string>();
			rooms = new Dictionary<string, List<string>>();
        }
	
    
		
		//Instantiates the server part of the socket connection
		//creates a listener and waits for messages from client
        public void Run()
        {
            ip = IPAddress.Parse("127.0.0.1");
            TcpListener server = new TcpListener(ip, port);
            server.Start();
            Console.WriteLine("Server listening on port " + port);

			
            //Creates threads awaiting a new connection, whenever a client connects to server
			//loops indefinately until server is manually closed (with control+c)
            while (true) {
                Console.WriteLine("Server waiting for socket connection .. ");
                TcpClient client = server.AcceptTcpClient();
                Console.WriteLine("Connection successful!");

                Thread thread = new Thread(() => {
                    try {
                        this.Connect(client);
                    } catch (Exception e) {
                        Console.WriteLine("Error: Connection to client lost ??!" );
                        Console.WriteLine(e.Message);
                    }
                });
                thread.Start();
            }
        }

        private void Connect(TcpClient client) {
            StreamReader reader = new StreamReader(client.GetStream());
            StreamWriter writer = new StreamWriter(client.GetStream());
	
            string username = null;
            string command = null;
            bool success = false;
            string arg1 = null;
			//necessary if command is more that a single word long
            string arg2 = null;
            List<string> results = null;

            command = reader.ReadLine();
            username = reader.ReadLine();
            Console.WriteLine("Server connected with user: " + username);
            success = CreateUser(username);
            writer.WriteLine(success);
            writer.Flush();
            while (!success) {
                Console.WriteLine("Awaiting user registration ..");
                command = reader.ReadLine();
                username = reader.ReadLine();
                Console.WriteLine("Server connected with user: " + username);
                success = CreateUser(username);
                writer.WriteLine(success);
                writer.Flush();
            }

            //Accepts input from user
			//Will write true to client if successful
            try {
                while (client.Connected) {
                    Console.WriteLine("Waiting for a command...");
                    command = reader.ReadLine();
                    Console.WriteLine("User has entered command: " + command);
					
                    switch (command) {
						//creates room and adds it to dictionary of rooms
                        case "create":
                            arg1 = reader.ReadLine();
                            Console.WriteLine("Server recieved argument: " + arg1);                           
                            success = CreateRoom(arg1);          
                            writer.WriteLine(success);
                            writer.Flush();
                            break;
						//User 'enters' chatroom
						//increments number of users in room
						//client allows for text communication for users in chatroom
						
                        case "enter":
                            arg1 = reader.ReadLine();
                            Console.WriteLine("Received argument: " + arg1);
                            arg2 = reader.ReadLine();
                            Console.WriteLine("Received argument: " + arg2);                            
                            results = JoinRoom(arg1, arg2);
                            
                            if (results == null) {;
                                writer.WriteLine(false);
                                writer.Flush();
                                break;
                            }
                            writer.WriteLine(true);
                            writer.WriteLine(results.Count);
                            for (int c=0; c<results.Count; c ++) {
                                Console.WriteLine(results[c]);
                                writer.WriteLine(results[c]);
                            }
                            writer.Flush();
                            break;
						//User exits chatroom
                        case "leave":
                            arg1 = reader.ReadLine();
                            Console.WriteLine("Recieved argument: " + arg1);

                            
                            success = LeaveRoom(arg1);
                            writer.WriteLine(success);
                            writer.Flush();
                            break;
						//Provides list of current chatrooms saved in dictionary
                        case "list":
                            
                            results = getRooms();
                            
                            if (results == null) {
                                writer.WriteLine(false);
                                writer.Flush();
                                break;
                            }

                            writer.WriteLine(true);
							//Gives count of rooms to client
                            writer.WriteLine(results.Count);
                            
                            for (int c = 0; c < results.Count; c += 1) {                         
                                writer.WriteLine(results[c]);
                            }
                            writer.Flush();
                            break;
							
						//Sends message to client. Uses multiple readers. 
                        case "send":
                            arg1 = reader.ReadLine();
                            arg2 = reader.ReadLine();

                            
                            success = SendMessage(arg1, arg2);
                            writer.WriteLine(success);
                            writer.Flush();
                            break;
							
						//adds latest argument to list
                        case "update":
                            arg1 = reader.ReadLine();         
                            results = Update(arg1);
                            
                            if (results == null) {
                                writer.WriteLine(false);
                                writer.Flush();
                                break;
                            }
                            writer.WriteLine(true);
                            writer.WriteLine(results.Count);

                            for (int c = 0; c < results.Count; c += 1) {
                                writer.WriteLine(results[c]);
                            }
                            writer.Flush();
                            break;
                        default:
                           
                            writer.WriteLine(false);
                            writer.Flush();
                            Console.WriteLine("Please enter a proper command..");
                            break;
                    }
                }
				//Error. Closes socket.
            } catch (Exception e) {
                
                Console.WriteLine("Error: Socket's connection to client has been list..? ");
                Console.WriteLine(e.Message);

                LeaveRoom(username);
                DeleteUser(username);
            }
        }
		//Creates user and stores in dictionary
        public bool CreateUser(string username) {
            string room = null;
            if (!this.users.TryGetValue(username, out room)) {
                this.users.Add(username, null);
                return true;
            }

            return false;
        }
		
		//Deletes user, removes from dictionary
        public bool DeleteUser(string user) {
            string room = null;

            if (!this.users.TryGetValue(user, out room)) {
                return false;
            }
            this.users.Remove(user);        
            return true;
        }
		
		//Creates room, adds to dictionary
        public bool CreateRoom(string room) {
            List<string> tempR = null;

            if (!this.rooms.TryGetValue(room, out tempR)) {
                this.rooms.Add(room, new List<string>());
                return true;
            }

            return false;
        }
		
		//Adds user to room.
		public List<string> JoinRoom(string room, string username) {
			
            string tempRoom = null;
			List<string> r;
			//makes sure the rooms and user arent null
			 if ((!this.users.TryGetValue(username, out tempRoom))||(tempRoom != null)||(!this.rooms.TryGetValue(room, out r)))
			 {
				 return null;
			 }
            
			
			
			this.users[username] = room;
            this.rooms[room] = r;

            return r;
			
        }
		
		public bool LeaveRoom(string user) {
            string room = null;
            List<string> r;

            if (!this.users.TryGetValue(user, out room)) {
                return false;
            }
            if (room == null) {
                return false;
            }

            if (!this.rooms.TryGetValue(room, out r)) {
                return false;
            }
         

            this.users[user] = null; 
            this.rooms[room] = r;
            return true;
        }
		
		//Get all current chat rooms
        public List<string> getRooms() {
            int count = this.rooms.Keys.Count;
            List<string> rooms = new List<string>();
            List<string> r;

            foreach (string key in this.rooms.Keys) {
                r = this.rooms[key];
                rooms.Add(key);
            }

            return rooms;
        }
		
		//List all messages in user's current chat room
        public List<string> Update(string username) {
            string room = null;
            List<string> r;

			//Null data for some reason
            if ((!this.users.TryGetValue(username, out room))||(room==null)||(!this.rooms.TryGetValue(room, out r))) {
                return null;
            }
          

            return r;
        }
		
		public bool SendMessage(string username, string message) {
            string room = null;
            List<string> r;

            if (!this.users.TryGetValue(username, out room)) {
                return false;
            }
            if (room == null) {
                return false;
            }

            if (!this.rooms.TryGetValue(room, out r)) {
                return false;
            }

            r.Add(username + " -> " + message);
            this.rooms[room] = r;

            return true;
        }
		

		
		static void Main(string[] args)
        {
            Server server = new Server();
            server.Run();
        }
		
    }
}